<h1>Contributing</h1>
<p>We want to welcome and include as many contributions as possible. All edits from small typos and grammar corrections to larger undertakings of guides and how-tos can be submitted and reviewed.</p>
<p>Ideally all articles should adhere to our guidelines. The aim is to bring them up to a standard that makes them more accessible for more users. Having said that, we will try to take all contributions bringing them up to standard where necessary.</p>
<p>If you plan to create something larger, please consider speaking to either staff or someone on IRC first. They might be able to help fit your work into the bigger picture.</p>
<p>The Wiki is stored in Git as HTML. It can be found <a href="https://bitbucket.org/feralio/wiki">on BitBucket</a>.</p>
<p>If in doubt, check the source of other articles for examples.</p>

<h2>Guidelines</h2>
<p>All articles must:</p>
<ul>
    <li>Adhere to <a href="#html-formatting">HTML formatting guidelines</a>.</li>
    <li>Have a clear <a href="#target-audience">target audience</a> in mind.</li>
    <li>Adhere to <a href="#general-standards">general standards</a>.</li>
    <li>Adhere to <a href="#category-standards">category standards</a>.</li>
</ul>

<h3 id="html-formatting">HTML Formatting</h3>
<p>The Wiki is written in HTML5 without inline CSS nor JavaScript. This means content should stick closely to the site's style. Below are the intended usage of elements:</p>
<dl>
    <dt>&lt;p&gt;</dt>
    <dd>Paragraph. Most prose should be placed in a paragraph container.</dd>

    <dt>class="full"</dt>
    <dd>Most elements attempt to be more readable by default and therefore have a width. This class will remove the width. Useful when a page element should use all available space i.e., non-text.</dd>

    <dt>class="nontext"</dt>
    <dd>Removes text-justification if applied to an element. Useful for non-text.</dd>

    <dt>&lt;ul, ol, li&gt;</dt>
    <dd>Lists should be used to separate content (making it more readable). Automatic indentation and bullet points.</dd>

    <dt>&lt;dl, dt, dd&gt;</dt>
    <dd>Definition lists. Useful for a list of key, value relationships with no row-based presentational structure.</dd>

    <dt>&lt;dl class="kv"&gt;</dt>
    <dd>Makes the definition-list more dense for better displaying of key / values on a single line.</dd>

    <dt>class="no-margin"</dt>
    <dd>Remove left-hand indententation (useful on lists).</dd>

    <dt>&lt;table, tbody, tr, td&gt;</dt>
    <dd>Tabular data. Useful for presenting rows of data with options. Not to be used for formatting.</dd>

    <dt>&lt;thead, tfoot, th&gt;</dt>
    <dd>Column header and footer for a table. Both <samp>thead</samp> and <samp>tfoot</samp> should appear before <samp>tbody</samp>.</dd>

    <dt>&lt;h1&gt;</dt>
    <dd>Main article header. There should only be one occurrence.</dd>

    <dt>&lt;h2, h3, h4, h5, h6&gt;</dt>
    <dd>Hierarchical Headers for article sections. Each nested header belongs as a sub-group to the parent.</dd>

    <dt>&lt;pre&gt;</dt>
    <dd>Pre-formatted block-level (mult-line) text.</dd>

    <dt>&lt;blockquote&gt;</dt>
    <dd>Similar to <samp>pre</samp>: multi-line output quoting something someone has said.</dd>

    <dt>&lt;code&gt;</dt>
    <dd>Put around computer code. Surround in a <samp>pre</samp> block if mult-line.</dd>

    <dt>&lt;samp&gt;</dt>
    <dd>Inline "sample output" e.g., the result of a computer program or useful for single-line quotes. Surround in a <samp>pre</samp> or <samp>blockquote</samp> if multi-line.</dd>

    <dt>&lt;kbd&gt;</dt>
    <dd>Inline, user with commands and other user input. Surround in a <samp>pre</samp> or <samp>blockquote</samp> if multi-line.</dd>

    <dt>&lt;var&gt;</dt>
    <dd>Represents a variable. Intended for variables that wouldn't be handled as-is i.e. that the user is expected to change first.</dd>

    <dt>&lt;em&gt;, class="emphasis"</dt>
    <dd>Emphasise a word or short phrase.</dd>

    <dt>&lt;strong&gt;</dt>
    <dd>Strong emphasis. <samp>em</samp> is preferred and <samp>strong</samp> should be used sparingly.</dd>

    <dt>&lt;sup, sub&gt;</dt>
    <dd></dd>

    <dt>&lt;small&gt;, class="small"</dt>
    <dd>Inline <samp>aside</samp> element. Useful for an inline "Note: ..." .</dd>

    <dt>class="quiet"</dt>
    <dd>Reduces contrast on text. Useful for when a page element is required but not used most of the time.</dd>

    <dt>class="dense"</dt>
    <dd>Condenses text by reducing line-height.</dd>

    <dt>&lt;!-- ... --&gt;</dt>
    <dd>HTML comments must not be used under any circumstances.</dd>
</dl>
<p>Extravagant mark-up should be avoided and articles should be kept mostly to prose.</p>

<h3 id="target-audience">Target Audience</h3>
<p>All articles should keep in mind the technical level of the target audience. This helps to keep explanations and assumptions suitable. Below are relevant target audiences:</p>
<dl>
    <dt>Linux sysadmin</dt>
    <dd>Happiest in a headless environment. Has probably memorised too many --options. Only wants a brief explanation on why obscure options are used (and even then, only whether to work out if pulling up the manpage is worthwhile). Looking for brief, concise overview of why something is being done. Will likely look up unknown options, tools, concepts on their own.</dd>

    <dt>Power user</dt>
    <dd>Probably runs Linux (intentionally) somewhere in his / her home. Knows the basics utiltiies and options, is eager to know more and wishes for explanations on commands and options as well as the general overview.</dd>

    <dt>SSH user</dt>
    <dd>Is running software as a means to an end. He / she is looking for a specific action "install x", "restart y" and so forth. Happy to dive into a little bit of the unknown to get something done i.e., copy and pasting provided SSH commands.</dd>

    <dt>User</dt>
    <dd>Would prefer not to have to install any extra software (beyond an SFTP client). Wishes for all actions to be done via a web interface on the website.</dd>
</dl>

<h4>Picking a Target Audience</h4>
<p>Articles under the same category should mostly have similar target audiences. The audience will be appropriate for the category. Most getting started guides should fall under "User" with enough explanations to cover the assumptions (e.g., so an "SSH user" article can simply say SSH into the slot).</p>
<p>There should be very few articles (if any) directed at the "Linux sysadmin" audience e.g., only someone with a lot of prior knowledge will understand it easily. These articles are likely contributions from individuals who don't know about these guidelines and hasn't yet been cleaned up.</p>

<h3 id="general-standards">General Standards</h3>
<p>All articles must:</p>
<ul>
    <li>
        <p>Have correct spelling and grammar:</p>
        <ul>
            <li>both UK and US spellings are acceptable,</li>
            <li>pages should be consistent in their choice,</li>
            <li>and by nature of the business, UK spelling is preferred.</li>
        </ul>
    </li>
    <li>Have an introduction clearly stating the <a href="#target-audience">target audience</a>.</li>
    <li>State a clear goal in the introduction.</li>
    <li>
        <p>Fit into the Wiki hierarchical structure with the intention that the article can be found:</p>
        <ul>
            <li>top-down by visiting from starting index by fitting into an existing category or sub-dividing an older one,</li>
            <li>and side-to-side by being referenced by other links meaning the URL does not change.</li>
        </ul>
    </li>
    <li>Use hyperlinks as the engine of state (HATEOS). This means the article should use lots of links to necessary requirements ideally in the introduction so that the reader knows what is required beforehand.</li>
    <li><p>Not repeat itself (DRY):</p>
        <ul>
            <li>Another article should be linked instead of repeating its content.</li>
            <li>If linking inside an article (instead of the whole page) then consider moving that content to a separate article.</li>
        </ul>
    </li>
    <li>
        <p>Be self-contained. Which means:</p>
        <ul>
            <li>All supporting files (especially images and scripts) must be stored in the Wiki repository itself alongside the article's HTML.</li>
            <li>These supplemental files must be served via the same mechanism as the Wiki (i.e., don't use an external host).</li>
            <li>Supplemental files should be stored in a folder next to the <samp>.html</samp> file. e.g., this article can be found at <samp>src/wiki/contributing.html</samp> so a related image would be placed into <samp>src/wiki/contributing/related.png</samp>.</li>
            <li>Extra files that are not necessarily served may also be stored in this folder. This might include source code, originals or even notes arriving at the conclusions in the article. They should be removed once no longer relevant.</li>
        </ul>
    </li>
    <li>Lossless formats are preferred over lossy e.g., PNG over JPG.</li>
    <li>Be timeless e.g., not attribute or thank specific individuals for contributing and try not to reference specific events.</li>
    <li>Scripts should be non-interactive i.e., command-line options only.</li>
    <li>If uncertain around standards, an article should endeavour to be consistent.</li>
</ul>

<h3 id="category-standards">Category Standards</h3>
<p>TODO (the following are notes):</p>
<ul>
    <li>API + server URLs are the same so we're locked into our first choice of URL once an article is linked. Find another method?</li>
    <li>The first aim is to prepare for software articles (install, use, extra actions) with all having an "SSH user" target audience.</li>
    <li><p>Therefore first categories should probably be:</p>
        <ul>
            <li>Getting started (understanding the slot, site) @ user</li>
            <li>Articles to be an "SSH user" e.g., getting to grips with SSH @ user</li>
            <li>Software @ SSH user</li>
            <li>Wiki @ HTML user (err, another type of user)</li>
        </ul>
    </li>
    <li>How would existing FAQ pages fit into this?</li>
    <li>Should go through old FAQ plans.</li>
</ul>
